# docker-notes

Notes about docker while working through DIAMOL

> Docker in a Month of Lunches

github url: [diamol](https://github.com/sixeyed/diamol)

A Dockerfile is a simple script to package an application (a set of instructions). A Docker image is the output.

## KEY GOALS WHEN CONTAINERIZING
1. optimize Dockerfile
    - ensure builds with fewest layers as possible
    - ensure caches as many layers as possible
1. make sure image is portable
    - use **SAME** image when deploying between environments (QA, STAGING, LIVE, ETC)
    - make sure app can read configuration values from container

## Dockerfile instructions
- upcase by convention, not required
- FROM 
    - every image has to start from another image
    - FROM alpine:latest
- ENV 
    - sets values for env vars
    - ENV METHOD="POST"
- WORKDIR
    - creates directory in container image filesystem, and sets it to current working directory (cwd)
    - WORKDIR /web-ping
- COPY 
    - copies files or directories from local filesystem into container image
    - {source path} {target path}
    - COPY app.js .
    - COPY . .
- CMD
    - specifies command to be run when Docker starts a container from the image
    - CMD ["node", "web-ping/app.js"]

- commands in Dockerfile can appear in any order.
- each command results in new layer being created
- Dockerfile and image creation can be optimized by 
    - moving least likely to change commands at top
    - most volatile command at bottom
    - combine commands where possible 
        - single command to set multiple env vars for example
    - therefore layer cache is invalidated as late as possible
    
## Basic Commands

### Run a container
`docker container run diamol/ch02-hello-diamol`

### Open a shell on a container
`docker container run --interactive --tty diamol/base`

### List Running Containers
`docker container ls`

`CONTAINER ID        IMAGE                         COMMAND NAMES                  CREATED            STATUS                                                                                                                      
d61923217a2c        diamol/base                   "/bin/sh"                 21 minutes ago      Up 21 minutes`

- Container ID is the hostname of the container
- Shortened Container ID can be used to reference container in following commands

### List All Containers (running or not)
`docker container ls --all`

### Show Processes Running in Container
`docker container top d61`

### Show Logs From Container
`docker container logs d61`

### Show Details of Container (Filesystem, Network, etc...)
`docker container inspect d61`

### Show CPU, Network, Disk Usage of Running Container
`docker container stats d61`

### Run Container and Detach
`docker container run --detach --publish 8088:80 diamol/ch02-hello-diamol-web`

- publish 
    -  port 8088 is forwarded to port 80 in container**

### Delete All Containers
`docker container rm --force $(docker container ls --all --quiet)`

- Removes all containers w/o confirmation
- $() => sends output from embedded command as input to other command


### Run Command on Specified Container
`docker container exec 8874 ls /usr/local/apache2/htdocs`

### Copy Local File to Container
`docker container cp index-1.html 8874:usr/local/apache2/htdocs/index.html`

- change is local to container 
- image is unchanged
- cp can copy from container to local, or vice versa
    - cp {source} {destination}

    
## Working with Docker Images

### Pulling an image from a registry

- image servers are called 'registries'

`docker image pull diamol/cho3-web-ping`

### Run a container and give it a friendly name
`docker container run -d --name web-ping diamol/ch03-web-ping`

`docker container logs web-ping`

`docker container stop web-ping`

### run a container and set environment vars
`docker container run --env TARGET=google.com diamol/ch03-web-ping`

### build an image from Dockerfile

- assumes Dockerfile is in cwd
- tags image with 'web-ping'
- '.' means use current directory as 'context'

`docker image build --tag web-ping .`
`docker image ls 'w*'`

- build with 'v2' tag
`docker image build -t web-ping:v2 .`

### run container from image
`docker container run -e TARGET=google.com -e INTERVAL=5000 web-ping`

### understanding image history and image layers
- a docker image is a logical collection of image layers
- image layers can be shared between different images and containers
- image contains all of the packaged files which becomes the containers filesystem
- image also contains a lot of metadata about itself
- can view details of each layer, and the commands that built it
- each line in Dockerfile creates an image layer
`docker image history web-ping`

### show docker images
`docker image ls`
- size column displays logical size of image (sum total of image layers)

### show exact disk space images are using
`docker system df`




